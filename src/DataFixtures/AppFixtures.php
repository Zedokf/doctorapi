<?php

namespace App\DataFixtures;

use App\Entity\DoctorAvailability;
use App\Entity\Reservation;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker;
class AppFixtures extends Fixture
{
    private $userPasswordHasher;
 
    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create();

        /** UserAdmin With all time Same Identifier */
        $userAdmin = new User();
        $userAdmin->setEmail('admin@doctorapi.fr');
        $userAdmin->setLastName($faker->lastName());
        $userAdmin->setPassword($this->userPasswordHasher->hashPassword($userAdmin, "password"));
        $userAdmin->setRoles(["ROLE_ADMIN"]);
        $manager->persist($userAdmin);
        /** */
        $docteurRole = [];
        $userRole = [];
        $adminRole = [];
        
        for ($i = 0; $i < 15; $i++) {
            $user = new User();
            $user->setEmail($faker->email());
            $user->setLastName($faker->lastName());
            $user->setPassword($this->userPasswordHasher->hashPassword($user, "password"));
            
            if ($i < 4) {
                $user->setRoles(["ROLE_DOCTOR"]);
                $docteurRole[] = $user;
            } elseif ($i == 4) {
                $user->setRoles(["ROLE_ADMIN"]);
                $adminRole[] = $user;
            } else {
                $user->setRoles(["ROLE_USER"]);
                $userRole[] = $user;
            }
            
            $manager->persist($user);
        }
                
        $consultationTime = [10, 20, 25, 30, 40, 60];

        for ($i = 0; $i < 20; $i++) {
            $reservation = new Reservation();
            
            // Sélectionner un patient à partir des tableaux $userRole et $adminRole
            if ($i % 2 == 0 && $i < count($userRole)) {
                $reservation->setPatient($userRole[$i]);
            } else {
                $reservation->setPatient($adminRole[$i % count($adminRole)]);
            }
            
            // Sélectionner un docteur à partir du tableau $docteurRole
            $reservation->setDoctor($docteurRole[$i % count($docteurRole)]);
            
            // Définir reservedAt en tant que nouvelle DateTime
            $reservedAt = new \DateTime();
            $reservedAt->add(new \DateInterval('P'.($i+1).'D')); // Ajoute $i+1 jours
            $reservation->setReservedAt($reservedAt);
            
            // Sélectionner consultationTime aléatoirement
            $randomIndex = array_rand($consultationTime);
            $consultationTimeValue = $consultationTime[$randomIndex];
            $reservation->setConsultationTime($consultationTimeValue);
            
            $manager->persist($reservation);
        }

        $daysOfWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];

        foreach ($docteurRole as $doctor) {
            foreach ($daysOfWeek as $day) {
                $doctorAvailability = new DoctorAvailability();
                $doctorAvailability->setDoctor($doctor);
                $doctorAvailability->setDayOfWeek($day);
        
                // Setting a random start time between 8:00:00 and 15:00:00
                $startTime = new \DateTime();
                $startHours = rand(8, 15);
                $startTime->setTime($startHours, 0, 0);
                $doctorAvailability->setStartTime($startTime);
                
                // Setting a random end time greater than start time and not more than 23:59:00
                $endTime = clone $startTime;
                $endHours = rand($startHours + 1, 23);  // end time is at least one hour later than start time
                $endTime->setTime($endHours, rand(0, 59), 0); // minutes are random between 0 and 59
                $doctorAvailability->setEndTime($endTime);
                
                $manager->persist($doctorAvailability);
            }
        }
        $manager->flush();
        
        
        
    }
}
