<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ApiResource(
    formats: [
        'json' => ['application/json']
    ],
    operations: [
    new Get(
        normalizationContext: ['groups' => ['getUsers']],
        description: "See the list of all users (admin permission)",
    ),
    new Delete(),
    new Put(denormalizationContext: ['groups' => ['getUsers', 'getEdit']]),
    new Post(denormalizationContext: ['groups' => ['getUsers', 'getEdit']]),
    new GetCollection(
        normalizationContext: ['groups' => ['getUsers']],
        description: "See the list of all users (admin permission)"
    )
    ]
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["getUsers", "getEdit", "getReservations"])]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Groups(["getUsers", "getEdit"])]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    #[Groups(["getEdit"])]
    private ?string $plainPassword = null;


    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["getUsers", "getEdit"])]
    #[ApiFilter(SearchFilter::class, properties: ['lastName' => 'partial'])]
    private ?string $lastName = null;

    #[ORM\OneToMany(mappedBy: 'patient', targetEntity: Reservation::class, cascade: ["remove"], orphanRemoval: true)]
    #[ORM\JoinColumn(onDelete:"CASCADE")]
    private Collection $reservations;

    #[ORM\OneToMany(mappedBy: 'doctor', targetEntity: DoctorAvailability::class, orphanRemoval: true)]
    private Collection $doctorAvailabilities;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: DoctorAvailability::class)]
    private Collection $availabilities;
    

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
        $this->doctorAvailabilities = new ArrayCollection();
        $this->availabilities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    #[Groups(["getUsers", "getEdit"])]
    public function getRoles(): array
    {
        // $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        // $roles[] = 'ROLE_USER';

        return array_unique($this->roles);
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): static
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): static
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations->add($reservation);
            $reservation->setPatient($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): static
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getPatient() === $this) {
                $reservation->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * Get the value of plainPassword
     */ 
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Set the value of plainPassword
     *
     * @return  self
     */ 
    public function setPlainPassword(?string $plainPassword)
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * @return Collection<int, DoctorAvailability>
     */
    public function getDoctorAvailabilities(): Collection
    {
        return $this->doctorAvailabilities;
    }

    public function addDoctorAvailability(DoctorAvailability $doctorAvailability): static
    {
        if (!$this->doctorAvailabilities->contains($doctorAvailability)) {
            $this->doctorAvailabilities->add($doctorAvailability);
            $doctorAvailability->setDoctor($this);
        }

        return $this;
    }

    public function removeDoctorAvailability(DoctorAvailability $doctorAvailability): static
    {
        if ($this->doctorAvailabilities->removeElement($doctorAvailability)) {
            // set the owning side to null (unless already changed)
            if ($doctorAvailability->getDoctor() === $this) {
                $doctorAvailability->setDoctor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, DoctorAvailability>
     */
    public function getAvailabilities(): Collection
    {
        return $this->availabilities;
    }

    public function addAvailability(DoctorAvailability $availability): static
    {
        if (!$this->availabilities->contains($availability)) {
            $this->availabilities->add($availability);
            $availability->setDoctor($this);
        }

        return $this;
    }

    public function removeAvailability(DoctorAvailability $availability): static
    {
        if ($this->availabilities->removeElement($availability)) {
            // set the owning side to null (unless already changed)
            if ($availability->getDoctor() === $this) {
                $availability->setDoctor(null);
            }
        }

        return $this;
    }
    
}
