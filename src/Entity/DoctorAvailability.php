<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Put;
use App\Repository\DoctorAvailabilityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    formats: [
        'json' => ['application/json']
    ],
    operations: [
    new Get(
        normalizationContext: ['groups' => ['getAvailabilities', 'getDoctor']],
        description: "See the list of all DoctorAvailability (admin permission)",
    ),
    new Delete(),
    new Put(
        denormalizationContext: ['groups' => ['getAvailabilities', 'availabilitiesEdit']]),
    new Post(denormalizationContext: ['groups' => ['getAvailabilities', 'availabilitiesEdit']]),
    ]
)]
#[ORM\Entity(repositoryClass: DoctorAvailabilityRepository::class)]
class DoctorAvailability
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'doctorAvailabilities')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['getDoctor'])]
    private ?User $doctor = null;

    #[ORM\Column(length: 10)]
    #[Groups(['getAvailabilities', 'availabilitiesEdit'])]
    private ?string $dayOfWeek = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    #[Groups(['getAvailabilities', 'availabilitiesEdit'])]
    private ?\DateTimeInterface $startTime = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    #[Groups(['getAvailabilities', 'availabilitiesEdit'])]
    private ?\DateTimeInterface $endTime = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDoctor(): ?User
    {
        return $this->doctor;
    }

    public function setDoctor(?User $doctor): static
    {
        $this->doctor = $doctor;

        return $this;
    }

    public function getDayOfWeek(): ?string
    {
        return $this->dayOfWeek;
    }

    public function setDayOfWeek(string $dayOfWeek): static
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTimeInterface $startTime): static
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        return $this->endTime;
    }

    public function setEndTime(\DateTimeInterface $endTime): static
    {
        $this->endTime = $endTime;

        return $this;
    }
}
