<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\ReservationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Validator\Constraints as Assert;


#[ApiResource(
    // controller: ReservationController::class,
    formats: [
        'json' => ['application/json']
    ],
    operations: [
        new Get(
            normalizationContext: ['groups' => ['getReservations']],
            description: "See one user",
        ),
        new Delete(
            security: "is_granted('ROLE_ADMIN') or object.getPatient().getId() == user.getId() or object.getDoctor().getId() == user.getId()",
            securityMessage: "You are not allowed to delete this reservation",
        ),
        new Put(
            denormalizationContext: ['groups' => ['reservationEdit']],
        ),
        new Post(
            denormalizationContext: ['groups' => ['getReservations', 'reservationEdit']],
        ),
        new GetCollection(
            security: "is_granted('ROLE_ADMIN')",
            securityMessage: "You are not allowed to view all reservations",
            normalizationContext: ['groups' => ['getReservations']],
            description: "See the list of the reservations (admin permission)"
        )
    ]
)]
#[ORM\Entity(repositoryClass: ReservationRepository::class)]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['getReservations'])]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'reservations')]
    #[ORM\JoinColumn(name: 'patient_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['getReservations'])]
    #[ApiFilter(SearchFilter::class, properties: ['patient.id' => 'exact'])]
    private ?User $patient = null;

    #[ORM\ManyToOne(inversedBy: 'reservations')]
    #[Groups(['getReservations'])]
    #[ApiFilter(SearchFilter::class, properties: ['doctor.id' => 'exact'])]
    private ?User $doctor = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['getReservations', 'reservationEdit'])]
    private ?\DateTimeInterface $reservedAt = null;

    #[ORM\Column]
    #[Assert\GreaterThan(5)]
    #[Groups(['getReservations', 'reservationEdit'])]
    private ?int $consultationTime = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]    
    private ?\DateTimeInterface $createdAt = null;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPatient(): ?User
    {
        return $this->patient;
    }

    public function setPatient(?User $patient): static
    {
        $this->patient = $patient;

        return $this;
    }

    public function getDoctor(): ?User
    {
        return $this->doctor;
    }

    public function setDoctor(?User $doctor): static
    {
        $this->doctor = $doctor;

        return $this;
    }
 
    public function getReservedAt(): ?\DateTimeInterface
    {
        return $this->reservedAt;
    }

    public function setReservedAt(\DateTimeInterface $reservedAt): static
    {
        $this->reservedAt = $reservedAt;

        return $this;
    }

    public function getConsultationTime(): ?int
    {
        return $this->consultationTime;
    }

    public function setConsultationTime(int $consultationTime): static
    {
        $this->consultationTime = $consultationTime;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
