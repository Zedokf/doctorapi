<?php

namespace App\Repository;

use App\Entity\DoctorAvailability;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DoctorAvailability>
 *
 * @method DoctorAvailability|null find($id, $lockMode = null, $lockVersion = null)
 * @method DoctorAvailability|null findOneBy(array $criteria, array $orderBy = null)
 * @method DoctorAvailability[]    findAll()
 * @method DoctorAvailability[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DoctorAvailabilityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DoctorAvailability::class);
    }
    /**
     * Returns a boolean value according to the availability of doctors
     *
     * @param User $doctor
     * @param string $dayOfWeek
     * @param string $startTime
     * @return boolean
     */
    public function isDoctorAvailable(User $doctor, string $dayOfWeek, string $startTime): bool
    {
        $qb = $this->createQueryBuilder('da');
        $qb->select('COUNT(da)')
            ->andWhere('da.doctor = :doctor')
            ->andWhere('da.dayOfWeek = :dayOfWeek')
            ->andWhere('da.startTime <= :startTime')
            ->andWhere('da.endTime > :startTime')
            ->setParameter('doctor', $doctor)
            ->setParameter('dayOfWeek', $dayOfWeek)
            ->setParameter('startTime', $startTime);
    
        $count = $qb->getQuery()->getSingleScalarResult();
    
        return $count === 0 ? false : true;
    }
    
    
//    /**
//     * @return DoctorAvailability[] Returns an array of DoctorAvailability objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('d.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

}
