<?php

namespace App\Repository;

use App\Entity\Reservation;
use App\Entity\User;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Reservation>
 *
 * @method Reservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reservation[]    findAll()
 * @method Reservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reservation::class);
    }

    public function findAllWithPagination($page, $limit)
    {
        $qb = $this->createQueryBuilder('a')
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * Check by day
     *
     * @param User $doctor
     * @param \DateTimeInterface $date
     * @return boolean
     */
    public function hasDoctorReservationOnDate(User $doctor, \DateTimeInterface $date): bool
    {
        $startOfDay = new \DateTime($date->format('Y-m-d 00:00:00'));
        $endOfDay = new \DateTime($date->format('Y-m-d 23:59:59'));

        return $this->createQueryBuilder('r')
            ->select('COUNT(r.id)')
            ->andWhere('r.doctor = :doctor')
            ->andWhere('r.reservedAt >= :startOfDay')
            ->andWhere('r.reservedAt <= :endOfDay')
            ->setParameter('doctor', $doctor)
            ->setParameter('startOfDay', $startOfDay)
            ->setParameter('endOfDay', $endOfDay)
            ->getQuery()
            ->getSingleScalarResult() > 0;
    }

    /**
     * Verify if Doctor has one appointment
     *
     * @param User $doctor
     * @param \DateTimeInterface $startTime
     * @param integer $consultationTime
     * @return boolean
     */
    public function hasDoctorReservationInTimeSlot(User $doctor, \DateTimeInterface $startTime, int $consultationTime): bool
    {
        $startTime = \DateTime::createFromFormat('Y-m-d H:i:s', $startTime->format('Y-m-d H:i:s'));
        $endTime = clone $startTime;
        $endTime->modify('+' . $consultationTime . ' minutes');

        return $this->createQueryBuilder('r')
            ->select('COUNT(r.id)')
            ->andWhere('r.doctor = :doctor')
            ->andWhere('r.reservedAt < :endTime')
            ->andWhere(':startTime < (DATE_ADD(r.reservedAt, :interval, \'MINUTE\'))')
            ->setParameter('doctor', $doctor)
            ->setParameter('startTime', $startTime)
            ->setParameter('endTime', $endTime)
            ->setParameter('interval', $consultationTime)
            ->getQuery()
            ->getSingleScalarResult() > 0;
    }
}