<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Repository\DoctorAvailabilityRepository;
use App\Repository\ReservationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class ReservationController extends AbstractController
{
    /**
     * Get all reservations
     *
     * @param ReservationRepository $reservationRepository
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/api/reservations', name: 'api.getReservations', methods: ['GET'])]
    public function getReservations(
        ReservationRepository $reservationRepository,
        SerializerInterface $serializer,
        Request $request,
    ): JsonResponse {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 3);

        $reservationList = $reservationRepository->findAllWithPagination($page, $limit);
        $jsonUsersList = $serializer->serialize($reservationList, 'json', ['groups' => 'getReservations']);

        return new JsonResponse($jsonUsersList, Response::HTTP_OK, [], true);
    }

    /**
     * Create one reservation
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    #[Route('/api/reservations', name: 'api.createReservation', methods: ['POST'], priority: 1)]
    public function createReservation(
        Request $request,
        ValidatorInterface $validator,
        UserRepository $userRepository,
        ReservationRepository $reservationRepository,
        DoctorAvailabilityRepository $doctorAvailabilityRepository,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $content = json_decode($request->getContent(), true);

        $patientId = $content['patient']['id'] ?? null;
        $doctorId = $content['doctor']['id'] ?? null;
        $reservedAt = new \DateTime($content['reservedAt']);
        $consultationTime = $content['consultationTime'];

        $patient = $userRepository->find($patientId);
        if (!$patient) {
            return new JsonResponse(['error' => 'Le patient spécifié n\'existe pas.'], Response::HTTP_BAD_REQUEST);
        }

        $doctor = $userRepository->find($doctorId);
        if (!$doctor || !in_array('ROLE_DOCTOR', $doctor->getRoles())) {
            return new JsonResponse(['error' => 'Le docteur spécifié n\'existe pas ou n\'a pas le rôle ROLE_DOCTOR.'], Response::HTTP_BAD_REQUEST);
        }

        $dayOfWeek = $reservedAt->format('l');
        $startTime = $reservedAt->format('H:i:s');

        if (!$doctorAvailabilityRepository->isDoctorAvailable($doctor, $dayOfWeek, $startTime)) {
            return new JsonResponse(['error' => 'Le docteur n\'est pas disponible à cet horaire.'], Response::HTTP_BAD_REQUEST);
        }

        if ($reservationRepository->hasDoctorReservationInTimeSlot($doctor, $reservedAt, $consultationTime)) {
            return new JsonResponse(['error' => 'Le docteur a déjà une réservation dans ce créneau.'], Response::HTTP_BAD_REQUEST);
        }
        $reservation = new Reservation();
        $reservation->setPatient($patient);
        $reservation->setDoctor($doctor);
        $reservation->setReservedAt($reservedAt);
        $reservation->setConsultationTime($consultationTime);

        $errors = $validator->validate($reservation);
        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return new JsonResponse(['error' => $errorMessages], Response::HTTP_BAD_REQUEST);
        }

        $entityManager->persist($reservation);
        $entityManager->flush();

        return new JsonResponse(['message' => 'La réservation a été créée avec succès.'], Response::HTTP_CREATED);
    }



    /**
     * Delete reservation
     *
     * @param Reservation $reservation
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    #[Route('/api/reservations/{id}', name: 'api.deleteReservation', methods: ['DELETE'])]
    public function deleteReservation(
        Reservation $reservation,
        EntityManagerInterface $entityManager
    ): JsonResponse {

        $entityManager->remove($reservation);
        $entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Update reservation
     *
     * @param Request $request
     * @param Reservation $reservation
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    #[Route('/api/reservations/{id}', name: 'api.updateReservation', methods: ['PUT'], priority: 1)]
    public function updateReservation(
        Request $request,
        Reservation $reservation,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager,
        ReservationRepository $reservationRepository
    ): JsonResponse {
        $content = json_decode($request->getContent(), true);

        $currentUser = $this->getUser();
        $doctor = $reservation->getDoctor();
        $patient = $reservation->getPatient();
        $reservedAt = new \DateTime($content['reservedAt']);
        $consultationTime = $content['consultationTime'];

        $isAdmin = in_array('ROLE_ADMIN', $currentUser->getRoles());
        if ($currentUser !== $doctor && $currentUser !== $patient && !$isAdmin) {
            return new JsonResponse(['error' => 'Vous n\'êtes pas autorisé à modifier cette réservation.'], Response::HTTP_FORBIDDEN);
        }

        if ($reservationRepository->hasDoctorReservationInTimeSlot($doctor, $reservedAt, $consultationTime)) {
            return new JsonResponse(['error' => 'Le docteur a déjà une réservation dans ce créneau.'], Response::HTTP_BAD_REQUEST);
        }

        if (isset($content['reservedAt'])) {
            $reservation->setReservedAt(new \DateTime($content['reservedAt']));
        }
        if (isset($content['consultationTime'])) {
            $reservation->setConsultationTime($content['consultationTime']);
        }

        $errors = $validator->validate($reservation);
        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return new JsonResponse(['error' => $errorMessages], Response::HTTP_BAD_REQUEST);
        }

        $entityManager->flush();

        return new JsonResponse(['message' => 'La réservation a été mise à jour avec succès.'], Response::HTTP_OK);
    }
}
