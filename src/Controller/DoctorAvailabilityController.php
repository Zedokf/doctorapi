<?php

namespace App\Controller;

use App\Entity\DoctorAvailability;
use App\Entity\User;
use App\Repository\DoctorAvailabilityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DoctorAvailabilityController extends AbstractController
{

    #[Route('/api/doctoravailabilities', name: 'api.getAllAvailabilities', methods: ['GET'], priority: 1)]
    public function getAllAvailabilities(
        DoctorAvailabilityRepository $availabilityRepository,
        SerializerInterface $serializer,
    ): JsonResponse {
        $availabilities = $availabilityRepository->findAll();
        $jsonAvailabilities = $serializer->serialize($availabilities, 'json', ['groups' => 'getAvailabilities', 'getDoctor']);

        return new JsonResponse($jsonAvailabilities, Response::HTTP_OK, [], true);
    }
    
    #[Route('/api/doctoravailabilities/{id}', name: 'api.deleteAvailability', methods: ['DELETE'])]
    public function deleteAvailability(DoctorAvailability $availability, EntityManagerInterface $em, DoctorAvailability $doctor): JsonResponse
    {
        $currentUser = $this->getUser();
        $isDoctor = $doctor->getDoctor();
        $isAdmin = in_array('ROLE_ADMIN', $currentUser->getRoles());
        // Check if the user is admin or owner
        if ($currentUser !== $doctor && $currentUser !== $isDoctor && !$isAdmin) {
            return new JsonResponse(['error' => 'Vous ne pouvez pas supprimer ce créneau.'], Response::HTTP_FORBIDDEN);
        }
        $em->remove($availability);
        $em->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/api/doctoravailabilities/{id}', name: 'api.editAvailability', methods: ['PUT'], priority: 1)]
    public function editAvailability(
        Request $request,
        SerializerInterface $serializer,
        DoctorAvailability $currentAvailability,
        EntityManagerInterface $em,

    ): JsonResponse {
        $currentUser = $this->getUser();
        $isDoctor = $currentAvailability->getDoctor();
        $isAdmin = in_array('ROLE_ADMIN', $currentUser->getRoles());
        // Check if the user is admin or owner
        if ($currentUser !== $currentAvailability && $currentUser !== $isDoctor && !$isAdmin) {
            return new JsonResponse(['error' => 'Vous ne pouvez pas modifier ce créneau.'], Response::HTTP_FORBIDDEN);
        }
        $updateAvailability = $serializer->deserialize($request->getContent(), DoctorAvailability::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $currentAvailability]);
        $em->persist($updateAvailability);
        $em->flush();
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    #[Route('/api/doctoravailabilities', name: 'api.createAvailability', methods: ['POST'], priority: 1)]
    public function createAvailability(
        Request $request,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        EntityManagerInterface $em,
        Security $security
    ): JsonResponse {
        // Get the currently logged in user
        $doctor = $security->getUser();
    
        // Check if the user is a doctor
        if (!$doctor || !in_array('ROLE_DOCTOR', $doctor->getRoles())) {
            return new JsonResponse(['error' => 'Vous n\'êtes pas docteur ou vous n\'avez pas le rôle ROLE_DOCTOR.'], Response::HTTP_FORBIDDEN);
        }
    
        // Deserialize the request JSON data into a DoctorAvailability object
        $jsonData = $request->getContent();
        $availability = $serializer->deserialize($jsonData, DoctorAvailability::class, 'json');
    
        // Assign the current user as the doctor for the availability
        $availability->setDoctor($doctor);
    
        // Validate the DoctorAvailability object
        $violations = $validator->validate($availability);
        if (count($violations) > 0) {
            $errorMessages = [];
            foreach ($violations as $violation) {
                $errorMessages[$violation->getPropertyPath()] = $violation->getMessage();
            }
            return new JsonResponse($errorMessages, JsonResponse::HTTP_BAD_REQUEST);
        }
    
        // Persist the DoctorAvailability object
        $em->persist($availability);
        $em->flush();
    
        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }
    

    #[Route('/api/doctoravailabilities/{doctorId}', name: 'api.getDoctorAvailabilities', methods: ['GET'], priority: 1)]
    public function getDoctorAvailabilities(int $doctorId, DoctorAvailabilityRepository $availabilityRepository, EntityManagerInterface $em): JsonResponse
    {
        $doctor = $em->getRepository(User::class)->find($doctorId);
        if(!$doctor) {
            return new JsonResponse(['error' => 'Doctor not found'], Response::HTTP_NOT_FOUND);
        }
        
        $availabilities = $availabilityRepository->findBy(['doctor' => $doctor]);
        $formattedAvailabilities = [];
    
        foreach ($availabilities as $availability) {
            $formattedAvailabilities[] = [
                'dayOfWeek' => $availability->getDayOfWeek(),
                'startTime' => $availability->getStartTime()->format('H:i:s'),
                'endTime' => $availability->getEndTime()->format('H:i:s'),
            ];
        }
        
        return new JsonResponse($formattedAvailabilities, Response::HTTP_OK);
    }
    
    
}
