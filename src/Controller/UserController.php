<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{
    /**
     * See the list of all users (admin permission)
     *
     * @param UserRepository $userRepository
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/api/users', name: 'api.getAllUsers', methods: ['GET'])]
    public function getAllUsers(
        UserRepository $userRepository,
        SerializerInterface $serializer,
        Request $request,
    ): JsonResponse {
        if (!$this->isGranted('ROLE_ADMIN')) {
            $errorMessage = [
                'message' => 'Vous n\'avez pas les droits suffisants pour voir les utilisateurs'
            ];

            return new JsonResponse($errorMessage, Response::HTTP_FORBIDDEN);
        }

        $page = $request->get('page', 1);
        $limit = $request->get('limit', 3);

        $usersList = $userRepository->findAllWithPagination($page, $limit);
        $jsonUsersList = $serializer->serialize($usersList, 'json', ['groups' => 'getUsers']);

        return new JsonResponse($jsonUsersList, Response::HTTP_OK, [], true);
    }

    /**
     * GET user by ID
     *
     * @param User $user
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    #[Route('/api/users/{id}', name: 'api.detailUser', methods: ['GET'])]
    public function getDetails(User $user, SerializerInterface $serializer): JsonResponse
    {
        $jsonUser = $serializer->serialize($user, 'json', ['groups' => 'getUsers']);
        return new JsonResponse($jsonUser, Response::HTTP_OK, [], true);
    }

    /**
     * Delete one user by ID
     *
     * @param User $user
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    #[Route('/api/users/{id}', name: 'api.deleteUser', methods: ['DELETE'])]

    public function deleteUser(User $user, EntityManagerInterface $em): JsonResponse
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            $errorMessage = [
                'message' => 'Vous n\'avez pas les droits suffisants pour supprimer un utilisateur'
            ];
            return new JsonResponse($errorMessage, Response::HTTP_FORBIDDEN);
        }

        $em->remove($user);
        $em->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Update one user
     *
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param User $currentUser
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    #[Route('/api/users/{id}', name: 'api.editUser', methods: ['PUT'])]

    public function editUser(
        Request $request,
        SerializerInterface $serializer,
        User $currentUser,
        EntityManagerInterface $em,
    ): JsonResponse {

        if (!$this->isGranted('ROLE_ADMIN') && $this->getUser() !== $currentUser) {
            $errorMessage = [
                'message' => 'Vous n\'avez pas les droits suffisants pour supprimer un utilisateur'
            ];
            return new JsonResponse($errorMessage, Response::HTTP_FORBIDDEN);
        }
        $updateUser = $serializer->deserialize($request->getContent(), User::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $currentUser]);
        $em->persist($updateUser);
        $em->flush();
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * Create one user
     *
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    #[Route('/api/users', name: 'api.createUser', methods: ['POST'])]
    public function createUser(
        Request $request,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        EntityManagerInterface $em
    ): JsonResponse {
        $jsonData = $request->getContent();
        $user = $serializer->deserialize($jsonData, User::class, 'json');

        $violations = $validator->validate($user);
        if (count($violations) > 0) {
            $errorMessages = [];
            foreach ($violations as $violation) {
                $errorMessages[$violation->getPropertyPath()] = $violation->getMessage();
            }
            return new JsonResponse($errorMessages, JsonResponse::HTTP_BAD_REQUEST);
        }

        $em->persist($user);
        $em->flush();

        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }
}
