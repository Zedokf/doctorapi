<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

class UserControllerTest extends WebTestCase
{
    private ?EntityManagerInterface $entityManager = null;
    private ?KernelBrowser $client = null;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $this->entityManager = $this->client->getContainer()->get('doctrine')->getManager();
    }

    public function testDeleteUser()
    {
        $client = static::createClient();
        
        $user = $this->entityManager
            ->getRepository(User::class)
            ->findOneBy(['email' => 'stoltenberg.leland@lockman.org']);

            if (!$user) {
            $user = new User();
            $user->setEmail('stoltenberg.leland@lockman.org');
            $user->setPassword('password');

            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        $userId = $user->getId();
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2ODk0NTY4MDUsImV4cCI6MTY4OTQ2MDQwNSwicm9sZXMiOlsiUk9MRV9BRE1JTiJdLCJ1c2VybmFtZSI6ImFkbWluQGRvY3RvcmFwaS5mciJ9.tNYSK-oSjgqDIMgWYmZ-k-Wr5MfsbVCP7rG8z5cLfYYCLyBfOvLL50LbfQyFKPI_His4KyCpzuZA1XddaGtfg__z6OFWR648HnL1UCac2QC9ZbiPHYHCXn5a7AzbRuh9a7NU5o-EEJKAnOMfQN-ORrYE35NVQy5lIsXNs86NnLV4y5O1nFfmpvQG7poHq0_dOH55Hj7EtRLgzx-J9MxaAgmSaGZiZtP9hju05bHGyavPXar5TRI7SPhLEWepMTzL_v4ebJApyqCTGILe5zmi-7pZYEdEaCfF2ALwhbnAMmLxSHP3UByBcLYdHIxIN_hFx20sks9pj8w6V5a1vudgDrYsckTccjjcCYly77Xt1xQ0a-0Z5VuH_kJxGpyauav9-VKfXrO_mrd2k0S-9HJ4iWmkU4nBH58DwC84eW-2Vt6IphPjpsaUZ73X0XvGlj0BfJEf_Hw3rjh78Kd85dqb0Jlq3-Sma7kWyHLEpIO23-bEuqriDfMPwlSXy_M8u6NUk0Nk7Nusx9poiK-yJy3MEJrJOM0O780R0JtmIc1W9NfVJQMll28gsSqEVGKBuYIiD4XFqIxsNOhHnMKJaeUBZyd1T_SpUq4Hi_Qabn-GrHMt4wCwB21KBQviLloRZymHUO-c9VSBzHTTlN-woUaH6edCx29N2SPfUvq53HM1bo0';
        $this->client->request(
            'DELETE',
            '/api/users/1', 
            [], 
            [], 
            ['HTTP_Authorization' => 'Bearer '.$token],
        );
    

        $this->assertEquals(204, $client->getResponse()->getStatusCode());

        $deletedUser = $this->entityManager
            ->getRepository(User::class)
            ->find($userId);
        
        $this->assertNull($deletedUser);
    }
}
