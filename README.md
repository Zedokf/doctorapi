# doctorapi

## Introduction

Si vous n'avez pas de serveur web (Type XAMPP, WAMP) mais Docker installé, passez à l'étape Docker
Sinon
Assurez vous d'avoir un serveur PHP + 8.1 / Un serveur SQL / composer et la CLI Symfony

## Génération du token 

Créez le dossier jwt dans config/

Ouvrez une console gitbash et tapez : 

openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout

## Initialiser le projet

git clone git@gitlab.com:Zedokf/doctorapi.git

Tapez composer update

Modifiez le .env.local en fonction de vos besoins.

Voici un exemple : 
```
APP_ENV=dev
DATABASE_URL=mysql://root:password@db:3306/doctorapi

###> nelmio/cors-bundle ###
CORS_ALLOW_ORIGIN='^https?://(localhost|127\.0\.0\.1)(:[0-9]+)?$'
###< nelmio/cors-bundle ###

###> lexik/jwt-authentication-bundle ###
JWT_SECRET_KEY=%kernel.project_dir%/config/jwt/private.pem
JWT_PUBLIC_KEY=%kernel.project_dir%/config/jwt/public.pem
JWT_PASSPHRASE=password
###< lexik/jwt-authentication-bundle ###
```

## Lancement du projet 

Ouvrez un terminal tapez symfony serve


## Docker
Vous pouvez docker-compose up -d

## Gérer la base de données
Sans docker : php bin/console d:d:c
Avec docker : La base de données devrait se générer automatiquement
Sans docker : php bin/console doctrine:schema:update --force
Avec docker : docker exec -it app bash -c "php bin/console doctrine:schema:update --force"

## Fixtures Load
Sans docker : php bin/console d:f:l
Avec docker : docker exec -it app bash -c "php bin/console d:f:l"